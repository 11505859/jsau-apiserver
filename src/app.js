'use strict'

const express = require('express')
const app = express()
const morgan = require('morgan')
const port = 8081
const bunyan = require('bunyan')
const log = bunyan.createLogger({name:String})
const fs = require('fs')
const path = 'src/events.json'

app.use(morgan('dev'))
//req-request,res-response

app.use(express.json())
//app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

app.get('/', (req, res) => {
    res.send('helloWorld')
})

app.get('/info', (req, res) => {
    res.json('jsau-apiserver-1.0')
})

app.get('/events', (req, res)=>{
    //async-callback
    fs.readFile(path, 'utf8', (error, data)=>{
        if (error) {
            res.status(500).json({error:'Internal Server Error'})
        }
        res.json(JSON.parse(data))

    })
})

app.get('/events/:event_id', (req, res)=>{
    // async-promise-then
    fs.promises.readFile(path, 'utf8')
        .then(JSON.parse)
        .then((data)=>{
            if (req.params.event_id in data) {
                res.json(data[req.params.event_id])
            } else {
                res.status(404).json({error: 'Not Found'})
            }
        })
        .catch((error) => {
            res.status(500).json({error:'Internal Server Error'})
        })
})

app.post('/events/:event_id', (req, res)=>{
    // async-promise-then
    fs.promises.readFile(path, 'utf8')
        .then(JSON.parse)
        .then((data)=>{
            if (req.params.event_id <= 0) {
                res.status(401).json({error: 'Unauthorised'})
            } else if (!(req.params.event_id in data)) {
                data[req.params.event_id] = req.body
                // async-promise-then
                return fs.promises.writeFile(path, JSON.stringify(data))
                    .then(res.status(200).json({status:'ok'}))
                    .catch((error)=>{
                        res.status(500).json({error:'Internal Server Error'})
                    })
            } else {
                res.status(400).json({error: 'Bad Request'})
            }
        })
        .catch((error) => {
            res.status(500).json({error:'Internal Server Error'})
        })
})

app.delete('/events/:event_id', async(req, res)=>{
    try {
        // async-promise-async-await
        const data = await fs.promises.readFile(path, 'utf8')
        const myData = await JSON.parse(data)
        if (Object.keys(myData)[0] != req.params.event_id && req.params.event_id in myData) {
            try {
                delete myData[req.params.event_id]
                // async-promise-async-await
                await fs.promises.writeFile(path, JSON.stringify(myData))
                res.status(200).json({status:'ok'})
            } catch (err) {
                res.status(500).json({error:'Internal Server Error'})
            }
        } else {
            res.status(401).json({error: 'Unauthorised'})
        }
    } catch (error) {
        res.status(500).json({error:'Internal Server Error'})
    }
})

app.put('/events/:event_id', async(req, res)=>{
    try {
        // async-promise-async-await
        const data = await fs.promises.readFile('src/events.json', 'utf8')
        const myData = await JSON.parse(data)
        if (Object.keys(myData)[0] != req.params.event_id && req.params.event_id in myData && Object.keys(req.body).length != Object.keys(myData)[0].length) {
            myData[req.params.event_id].title = req.body.title
            myData[req.params.event_id].lieu = req.body.lieu
            myData[req.params.event_id].date = req.body.date
            // async-promise-async-await
            await fs.promises.writeFile(path, JSON.stringify(myData))
            res.status(200).json({status:'ok'})
        } else {
            res.status(401).json({error: 'Unauthorised'})
        }
    } catch (error) {
        res.status(500).json({error:'Internal Server Error'})
    }
})

app.listen(port, () => {
    log.info(`Example app listening at http://localhost:${port}`)
})
module.exports = app
