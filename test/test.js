'use strict'
const request = require('supertest')
const app = require('../src/app.js')

describe('test2  http methode', ()=>{
    describe('GET /', ()=>{
        it('should return 200', (done)=>{
            request(app)
                .get('/')
                .expect(200, done)
        })
    })

    describe('GET /info', ()=>{
        it('should return 200', (done)=>{
            request(app)
                .get('/info')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200, done)
        })
    })
    describe('GET /events', ()=>{
        it('should return 200', (done)=>{
            request(app)
                .get('/events')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200)
                .end((err, res)=>{
                    if (err) {
                        return done(err)
                    }
                    return done()
                })
        })
    })
    describe('GET /events/1', ()=>{
        it('should return 200', (done)=>{
            request(app)
                .get('/events/1')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('GET /events/6', ()=>{
        it('should return 404', (done)=>{
            request(app)
                .get('/events/6')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(404)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('POST /events/0', ()=>{
        it('should return 401', (done)=>{
            request(app)
                .post('/events/0')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(401)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('POST /events/1', ()=>{
        it('should return 400', (done)=>{
            request(app)
                .post('/events/1')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(400)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('POST /events/3', ()=>{
        it('should return 200', (done)=>{
            request(app)
                .post('/events/3')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .send({title:'eng', lieu:'Villetaneuse', date:'21/11/21'})
                .expect(200)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('DELETE /events/0', ()=>{
        it('should return 401', (done)=>{
            request(app)
                .delete('/events/0')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(401)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('DELETE /events/1', ()=>{
        it('should return 401', (done)=>{
            request(app)
                .delete('/events/1')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(401)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('DELETE /events/3', ()=>{
        it('should return 200', (done)=>{
            request(app)
                .delete('/events/3')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('PUT /events/1', ()=>{
        it('should return 401', (done)=>{
            request(app)
                .put('/events/1')
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(401)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
    describe('PUT /events/2', ()=>{
        it('should return 404', (done)=>{
            request(app)
                .put('/events/2')
                .send({title:'tec', lieu:'Villetaneuse', date:'21/11/21'})
                .expect('Content-Type', 'application/json; charset=utf-8')
                .expect(200)
                .end((err, res)=>{
                    if (err) {
                        done(err)
                    }
                    done()
                })
        })
    })
})
