# jsau-apiserver

[![pipeline status](https://gitlab.sorbonne-paris-nord.fr/11505859/jsau-apiserver/badges/main/pipeline.svg)](https://gitlab.sorbonne-paris-nord.fr/11505859/jsau-apiserver/-/commits/main)

[![coverage report](https://gitlab.sorbonne-paris-nord.fr/11505859/jsau-apiserver/badges/main/coverage.svg)](https://gitlab.sorbonne-paris-nord.fr/11505859/jsau-apiserver/-/commits/main)

### Prerequisite

NodeJS should be installed.

Once installed launch these command:

- `aptitude install jq`
- `npm ci`
- `npm run test`

### Example of execution
Open two different terminal in the first terminal you should run this command `npm run start:watch` and in the another terminal  you should run these command one by one
- To see all the events in the database: `curl http://localhost:8081/events`
- To create a new event: `curl -X POST -H 'Content-Type: application/json' --data '{"title":"test","lieu":"Villetaneuse","date":"22/01/22"}' http://localhost:8081/events/3`
- To see the created event: `curl http://localhost:8081/events/3 | jq .`
- To modifie the created event: `curl -X PUT -H 'Content-Type: application/json' --data '{"title":"eng","lieu":"Villetaneuse","date":"21/11/22"}' http://localhost:8081/events/3`
- To delete the created event `curl -X DELETE -H 'Content-Type':application/json http://localhost:8081/events/3`

### Continuation of other projects
[jsau-webserver](https://gitlab.sorbonne-paris-nord.fr/11505859/jsau-webserver)

[jsau-webapp](https://gitlab.sorbonne-paris-nord.fr/11505859/jsau-webapp)
